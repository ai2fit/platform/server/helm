{{/*
Expand the name of the chart.
*/}}
{{- define "singularity-execution-environment.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "singularity-execution-environment.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "singularity-execution-environment.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "singularity-execution-environment.labels" -}}
helm.sh/chart: {{ include "singularity-execution-environment.chart" . }}
{{ include "singularity-execution-environment.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "singularity-execution-environment.selectorLabels" -}}
app.kubernetes.io/name: {{ include "singularity-execution-environment.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "singularity-execution-environment.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "singularity-execution-environment.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the role to use
*/}}
{{- define "singularity-execution-environment.roleName" -}}
{{- if .Values.role.create }}
{{- default (print (include "singularity-execution-environment.fullname" .) "-execution-access") .Values.role.name }}
{{- else }}
{{- default "default" .Values.role.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the role to use
*/}}
{{- define "singularity-execution-environment.roleBindingName" -}}
{{- if .Values.roleBinding.create }}
{{- default (include "singularity-execution-environment.roleName" .) .Values.roleBinding.name }}
{{- else }}
{{- default "default" .Values.roleBinding.name }}
{{- end }}
{{- end }}

{{/*
Module storage PVC name
*/}}
{{- define "singularity-execution-environment.moduleStoragePvcName" -}}
{{- if .Values.volumes.moduleStorage.existingPersistentVolumeClaimName }}
{{- .Values.volumes.moduleStorage.existingPersistentVolumeClaimName }}
{{- else }}
{{- .Values.volumes.moduleStorage.name }}
{{- end }}
{{- end }}