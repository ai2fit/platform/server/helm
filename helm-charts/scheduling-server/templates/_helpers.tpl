{{/*
Expand the name of the chart.
*/}}
{{- define "scheduling-server.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "scheduling-server.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "scheduling-server.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "scheduling-server.labels" -}}
helm.sh/chart: {{ include "scheduling-server.chart" . }}
{{ include "scheduling-server.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "scheduling-server.selectorLabels" -}}
app.kubernetes.io/name: {{ include "scheduling-server.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "scheduling-server.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "scheduling-server.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the role to use
*/}}
{{- define "scheduling-server.roleName" -}}
{{- if .Values.role.create }}
{{- default (print (include "scheduling-server.fullname" .) "-execution-access") .Values.role.name }}
{{- else }}
{{- default "default" .Values.role.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the role to use
*/}}
{{- define "scheduling-server.roleBindingName" -}}
{{- if .Values.roleBinding.create }}
{{- default (include "scheduling-server.roleName" .) .Values.roleBinding.name }}
{{- else }}
{{- default "default" .Values.roleBinding.name }}
{{- end }}
{{- end }}

{{/*
Module storage PVC name
*/}}
{{- define "scheduling-server.moduleStoragePvcName" -}}
{{- if .Values.volumes.moduleStorage.existingPersistentVolumeClaimName }}
{{- .Values.volumes.moduleStorage.existingPersistentVolumeClaimName }}
{{- else }}
{{- .Values.volumes.moduleStorage.name }}
{{- end }}
{{- end }}